# zOS v0.6
### Real-time Operating System for the SX Microcontroller
zOS is a real-time operating system (RTOS) developed for the [SX28](https://www.parallax.com/sites/default/files/downloads/SX20AC-SX28AC-Datasheet-v1.7.pdf) microcontroller from [Parallax](http://www.parallax.com). The RTOS was created as a final project for the course ENPM 609 "Microprocessor-based Design," taught by [Prof. Manoj Franklin](https://www.ece.umd.edu/faculty/franklin) at the [University of Maryland](http://eng.umd.edu).

The inspiration to create zOS came from the open-source project [cxlib](http://uhaweb.hartford.edu/kmhill/projects/cxlib/index.htm) and the publicly available coursework "[Super loops in 9S12 Assembly Language](http://uhaweb.hartford.edu/kmhill/suppnotes/SuperLoop/SuperLoop9S12.htm)", both created by [Prof. Krista Hill](http://www.hartford.edu/ceta/about-us/facultystaff/ece/hill.aspx) at the [University of Hartford](http://www.hartford.edu/ceta).

The project was written in the [SX-Key Editor v3.3.0](https://www.parallax.com/downloads/sx-key-editor-software) and compiled with the SX/B Compiler v1.51.03.

### About zOS

 * Up to seven independent tasks can be implemented.
 * Tasks can be periodic, aperiodic, and sporadic.
 * Task priority is static.
     * Tasks execute in sequential order, from Task 1 to Task 7.
     * Task schedulability must be performed by the developer.
 * RTOS period is 8 milliseconds.
     * Task execution deadline: 8 milliseconds.
     * Any remaining time is considered slack; no execution is performed.
 * RTOS will not fault if a deadline is missed (by design).
     * Missed periods will *not* be executed once the processor is available.
 * Architecture allows for periodic tasks, aperiodic tasks, and event-driven tasks.
     * Periodic tasks can execute up to ~125 Hz.
     * Aperiodic tasks can change periodicity on each execution.
     * Event-driven tasks *must* be enabled by another executing task or a microcontroller interrupt.
     * Lower priority event-driven tasks will be executed during same period they are enabled.

### General RTOS Overview
An example of an RTOS with clock-scheduled task execution is shown below in Figure 1, and is representative of how zOS executes.

At the high level, there are two potential states zOS may be in: the WORK state, or the SLACK state. The WORK state is when zOS is executing code (e.g., user code, operating system code). The SLACK state is when zOS has completed all work for the current period (T) and is waiting for the next Real Time Clock (RTCC) event to trigger the next period.

During the WORK state, the RTOS executes all tasks that are scheduled to run. This is shown in the expanded timeline at the bottom of Figure 1. In this case, three tasks are scheduled to run. When Task 1 completes, the RTOS takes over to perform a 'context switch' (represented in Figure 1 as "R") before executing the next task (Task 2). This process repeats until all scheduled tasks have executed. At this point, the RTOS enters the SLACK state, until the next RTCC event.

**Figure 1. Timeline showing RTCC interrupts, work time, slack time, and a detailed view of work.**

![Figure 1](./Images/RTOS_Timeline.png)

When a task finishes its execution, the RTOS will store the 'context' (the current state of important processor registers). One very important register that is stored is the Program Counter (PC), which keeps track of the current program memory location being executed. When the same task needs to execute during a future period, the RTOS will start executing the task from the previously stored PC (technically, PC+1), allowing the task to continue whatever it was doing before the context switch.

This type of execution gives us a few benefits:

 * Tasks created for control systems can rely on consistent timing.
 * A single-CPU microcontroller, like the SX, can give the illusion of having multiple processors.
 * Tasks that run infrequently, or have a lower priority, can be properly scheduled to avoid performance impacts to other more frequent or higher priority tasks.
 * Task code is kept neatly organized in independent files and does not need to be intertwined with code from other tasks.
 * Tasks can contain looping structures, as long as they properly handle context switches while executing the loop.
 * If a task does not have the resources it needs while executing, it can pass control to the next task while it waits.
 * If no tasks are scheduled, the RTOS can quickly return to the SLACK state; this can be especially beneficial to low-power systems (e.g., battery operated).


### zOS Implementation

zOS is implemented within the following files, all of which are required for proper operation:

 * `RTOS.SRC` - Main RTOS code, initialization
 * `Macros.SRC` - Useful RTOS macros
 * `TASK<x>.SRC` - User code for each task

Most developers need only interact with the task source files. Tasks created in zOS can be: periodic, aperiodic, or sporadic.

 * Periodic tasks - execute at a constant frequency.
 * Aperiodic tasks - execute at a different frequencies, configurable during run time.
 * Sporadic tasks - triggered by events (from other tasks or microcontroller interrupts).

How a task executes is determined by the task during run time. As described in the previous sections, the RTOS does not force any scheduling type on a task. For example, a task may be disabled until an event occurs, which causes the task to enter a periodic cycle for 10 seconds, before finally disabling itself.

An example task is shown below along with the typical supporting code. This example task stores the value 5 into the task variable `t4_var`, decrements the variable until it equals 0, and finally returns control to the RTOS. Task variables are defined immediately after the `ORG $95` instruction. Task code should only be added between the `TASK<x>` label and the `TASK<X>_END` label. Any code outside of these labels will be overwritten during compilation.

```
Task4_TCB_PC      EQU $90
Task4_TCB_STATUS  EQU $91
Task4_TCB_FSR     EQU $92
Task4_TCB_Counter EQU $93

Task4_Period      EQU 1

ORG $95                   ; Task variables go here
t4_var  ds 1

ORG TASK3_END
TASK4                     ; Task code goes here
  mov  t4_var, #$05
:loop
  dec  t4_var
  jnz  :loop
  TaskSwitch              ; All done--return control to the RTOS
  jmp  TASK4
TASK4_END

Task4_PC_Ptr      EQU TASK4&$0FF
```

#### Task Context Switches
Two macros are provided to perform context switches:

 * `TaskSleep <periods>` - Sleeps the task for the specified number of RTOS periods.
 * `TaskSwitch` - Give control back to the RTOS (conceptually equivalent to `TaskSleep 0`).

For example, `TaskSleep 10` will sleep the task for 10 RTOS periods, i.e. 80 milliseconds (10 periods * 8 millisecond RTOS period = 80 milliseconds). It is important to note the RTOS will *not* halt an executing task if it takes too long (i.e., the deadline will not be met). Plan for the worst-case execution time (WCET).

Example usage of the `TaskSwitch` macro is shown in the snippet below. This task is enabled dynamically by another task after an external I/O event occurs. Notice how `disable_task2` is called before `TaskSwitch`, since the task is only supposed to execute once after the event is detected.

```
:task2_loop
  turn_off_display                   ; Turn off the display during update
  mov  w, rtc_seconds                ; Move the global seconds register to W reg
  call decode_digit                  ; Translate the value to LED segments
  mov  segments, w                   ; Write these values into ra port
  turn_on_display                    ; Segments have been written, turn on the display
  disable_task2                      ; After updating the display, disable this task
  TaskSwitch                         ; Task is not cyclic, so simply return control to the RTOS
  jmp  :task2_loop
```

#### Enabling and Disabling Tasks
Two macros are provided for enabling and disabling tasks:

 * `enable_task<x>`
 * `disable_task<x>`

where `x` is equal to the task number to be enabled or disabled. Tasks are *disabled* by default; you must insert an `enable_task<x>` instruction in the `RTOS.SRC` initialization section or in another task. NOTE: `enable_task` macros are already provided for each task in the RTOS initialization section.

Non-periodic tasks can be enabled and disabled by running tasks using the same  `enable_task<x>` and `disable_task<x>` macros. Also, `disable_task<x>` can be executed within a non-periodic task to disable itself during run time without issue.

# Technical Details

### How do the tasks store their context?
zOS only allows up to 7 tasks because of the SX28's memory organization. The figure below shows the memory structure of the SX28. Notice the 8 separate banks of memory, each with 16 bytes of SRAM. To make it easier for each task to store its context, it was decided to dedicate one bank of SRAM for each task. Bank 0 of SRAM is reserved for the RTOS.

The lowest five bytes of each SRAM bank hold the task control block (TCB) for the task. While small tasks will likely operate fine with only 11 bytes of RAM, larger tasks may have an issue. This can be solved by utilizing RAM allocated for other tasks, if they are not implemented, by utilizing indirect addressing. Tasks requiring that much RAM probably shouldn't be implemented on the SX.

**Figure 2. Memory organization of zOS on the SX28 microcontroller.**

![Figure 2](./Images/SX28_Memory_Org.PNG)

The TCB contains the following registers:

 * Program Counter (PC) - Next address to execute within the task context.
 * STATUS Register - Determines which SRAM bank is active. Also holds TO, PD, Z, DC, and C flags.
 * FILE SELECT Register (FSR) - Used for indirect addressing.
 * Counter - Decremented by the RTOS on each period. Task is executed when counter == 0.

Figure 3 below shows the zOS memory allocation overlaid on the SX memory structure. The page structure for Task 1 has been expanded for clarity. At this time, the reserved memory byte (TCB relative address 0x04) is not utilized by zOS.

**Figure 3. How zOS uses the SX28 memory.**

![Figure 3](./Images/zOS_Mem.png)

You may have noticed a relative lack of registers within the TCB, as compared to other well known microprocessors (e.g., r0, r1, r2, etc.). The SX only has one global processor register, W, and it is used for working data; the SX does not implement any temporary registers. The reason why the W register is not included in the TCB is described in the section "Known Issues, Technicalities, and Thoughts".

Although there are not any explicitly specified registers, RAM addresses 0x07 through 0x0F are globally accessible, so they can be used as temporary registers (zOS actually allocates two temporary registers, W2 and W3). However, they are not included in the context due to the limited amount of RAM available. Any temporary registers a task wishes to use should be allocated within its designated RAM bank. There is no difference in the required access time, when compared to using global RAM. Careful design and consideration by the developer can mitigate most of these issues.

It can be argued that zOS does not actually store context at all, but simply stores the register configurations and PC values required to re-enter the task.

### How are tasks executed?
Upon receiving the RTCC interrupt, zOS exits the IDLE loop and executes tasks from 1 to 7 (Task 1 has highest priority). A screenshot of a early version of zOS executing tasks 1 through 5 is shown in Figure 4. In this figure, it is easy to see the amount of overhead required for zOS (the space between any two tasks), granted the tasks used in this example were extremely fast. The context switch time between tasks for zOS v0.6 is 65 microseconds at 4 MHz.

Figure 5 shown below shows the same tasks but on a larger time scale, showing the slack time between periods, and total time the RTOS is active (the red signal on channel D0).

**Figure 4. Logic analyzer screenshot of zOS executing tasks 1 through 5.**

![Figure 4](./Images/NewFile1.bmp)

**Figure 5. Zoomed-out view of previous figure to show slack time between periods.**

![Figure 5](./Images/NewFile0.bmp)

When a task performs a context switch using the provided RTOS macros, the task control block (TCB) is updated with the defined number of RTOS periods the task wishes to remain idle. Upon starting each period, zOS will go through each TCB and decrement its counter, and if it equals 0, will add it to the register for tracking tasks to be executed.

If Task 1 and Task 2 are both scheduled to execute during the current RTOS period, both tasks *will* execute, regardless of how much time the tasks require to complete. It is up to the developer to guarantee that high priority code is written in the lower-value tasks (e.g., Task 1, Task 2). Higher-priority should be given to tasks that:

 * execute at higher frequencies,
 * generate dependencies for other tasks,
 * have short response times (do not interact with slow peripherals),
 * require minimal jitter, or
 * take less time to execute.

The optimal scheduling policy is dependent on the application.

#### A more-technical description

Within zOS, there are two registers used to determine if a task wants to execute: the ENABLED register, and the TASK_READY register (each task is represented as a bit). These two registers are combined with an AND instruction; the result informs zOS which tasks should be executed during the current period. The TASK_READY register is updated on every context switch for two reasons: it allows the zOS to perform a single-cycle instruction to determine if more tasks need to be executed, and it allows high priority tasks to enable lower-priority tasks to execute during the same period.

**Figure 6. Flow chart of task execution.**

![Figure 6](./Images/FlowChart.png)

### Can a task run initialization code?
Yes. This can be performed with careful usage of labels. On the first execution of the task, zOS will execute the code starting at the PC value of the TASK<X> label. Initialization code can be placed immediately following this label. If another label is placed after the initialization code (e.g., TASK<X>\_LOOP), the main task loop can use it instead of the TASK<X> label.

An example of initialization code with a separate task loop is shown in TASK1.SRC.  

NOTE: Be sure to determine the execution time of the initialization code to guarantee the deadline is not missed.

# Example Tasks
The zOS source code comes preprogrammed with the project code used for the ENPM 609 course with Prof. Manoj Franklin. Three tasks are populated:

 * Task 1 - Communicates with DS1302 Real-time clock module from Velleman
 * Task 2 - Writes the RTC seconds to a seven segment display
 * Task 3 - Listens to a button on the SX Tech breadboard, resets the RTC seconds if pressed

For more-detailed information than provided below, please reference the inline comments within the individual Task files.

### Implementation

The board is fitted with a real-time clock (RTC), seven segment display, and a button for the three tasks. No resonator is populated, as the code utilizes the SX's internal oscillator.

**Figure 7. Implementation of the example code on the SX Tech board.**

![Figure 7](./Images/ProjectProto.jpg)

#### Task 1

Upon initialization, Task 1 writes the value 00 to the seconds register within the RTC. The task executes every 10 periods (80 milliseconds) to read back the current value of seconds. If the value has changed, it enables task 2 (to update the seven segment display). The task also checks to see if the button is pressed by checking a global register set by Task 3. If so, it will reset the RTC seconds to 00. It does this by jumping to the initialization code.

SX-Tech board connections:

* RA.0 - Chip Select (CS)
* RA.1 - Serial Data (D)
* RA.2 - Serial Clock (CLK)

**Figure 8. Task 1 requesting and receiving time from DS1302 RTC (D0=CS, D1=D, D2=CLK).**

![Figure 8](./Images/NewFile5.bmp)

#### Task 2

This task is disabled by default, and is activated by Task 1 receiving an updated seconds value from the RTC. When enabled, the task will turn off the display (by setting the common anode output pin to LOW), updates the RB register (segment driver pins) with the proper bits for the current seconds value, and turns the display back on. After this job completes, the task disables itself so it does not run again.

SX-Tech board connections:

 * RC.0 - Common Anode
 * RB.0 - Decimal Point
 * RB.1 - Segment C
 * RB.2 - Segment D
 * RB.3 - Segment E
 * RB.4 - Segment B
 * RB.5 - Segment A
 * RB.6 - Segment F
 * RB.7 - Segment G

#### Task 3

If the task detects the button is depressed, the task sleeps for ~40 milliseconds for debounce. If upon reentry the task detects the button is still depressed, it updates the global register used to inform Task 1 to reset the seconds to 00. The task will then wait until the button is released to return to the initial state. This stops a user from continuously resetting the seconds while the button is still depressed.

SX-Tech board connections:

 * RC.1 - Button Value (pulled HIGH with external 10k ohm resistor)

#### Task Execution Examples

**Figure 9. No tasks were scheduled to execute during this period (zOS activity shown on D0).**

![Figure 9](./Images/NewFile12.bmp)

**Figure 10. Task 1 reading from the RTC (D1), and Task 3 checking the status of the button (D3).**

![Figure 10](./Images/NewFile9.bmp)

**Figure 11. Task 1 detects the seconds have changed (D1), Task 2 updating the display (D2), and Task 3 checking the status of the button (D3).**

![Figure 11](./Images/NewFile10.bmp)

**Figure 12. Task 1 writing new seconds, followed by an immediate read back (D1), Task 2 updating the display (D2), and Task 3 checking the status of the button (D3).**

![Figure 12](./Images/NewFile11.bmp)

# Known Issues, Technicalities, and Thoughts

 * It was discovered during testing that the W register is used when executing the JMP command. This meant that during a context switch, the W register would be overwritten with the PC address to perform the jump. This caused the removal of the W register from the TCB (hence the RESERVED byte in the TCB). A macro could be created to allow a developer to restore the W register *after* the jump is performed, but this was beyond the scope of zOS 0.6. So many of the processor's instructions use the W register (i.e., the register is constantly being changed), it was deemed not worth providing this function.

 * The SX provides 4096 bytes ROM, which requires 12-bits for addressing. However, the SX does not provide the developer with access to the upper four bits (it is an 8-bit processor). Normally this is handled by the developer using the PAGE instruction. A bigger issue arose when it was realized task code would likely span multiple pages of ROM, requiring the RTOS to know which page the task code resides, even though the RTOS cannot *set* the required bits. To resolve this problem, the EQU directive was used to inform the compiler where each task label memory location is at compile-time (e.g., `Task3_PC_Ptr EQU TASK3&$0FF`).

 * Splitting the tasks into separate files also turned out to be a chore. The issue was, a developer obviously wants to define variables for the task within the same file the task code is in. However, each time the ORG directive is executed to allocate space for the RAM variables, you must know the last ROM memory location used for the previous task. To resolve this problem, ORG directives were loaded with compile-time labels, allowing the compiler to properly organize the ROM based on its compiled space requirements.

 * The SX has a weird quirk that CALL instructions cannot be pointed at subroutines existing within the second half of a ROM page. In my development I used CALLs sparingly because of this. This has to do with the fact that the 8th bit of the PC is cleared when performing the CALL instruction (see p.163 of the SX-Key/Blitz Manual).

 * In Task 2, a `jmp  pc+w` instruction is used to quickly determine the LED segments to be enabled for a specific numeric value. However, it was later realized that corrupt data can cause the `jmp` instruction to potentially jump to a location further up in ROM (e.g., Task 3), which could cause an infinite loop or catastrophic failure. Data validation should be performed on the W register before the `pc+w` jump is performed.

 * A lot of the macros are statically defined, with some macros being redefined for each task. These macros can likely be rewritten to use compile-time parameters for code efficiency. However, this will not reduce the compiled code size.

 * When the RTOS period begins, the delay counter in each task TCB is decremented. It was realized later in development that zOS should decrement a counter *ONLY* if the task is enabled. This feature would allow for tasks to pre-define a period to sleep *after* an event occurs that causes them to be enabled (e.g., execute Task 4 50 milliseconds after it is enabled).

 * Just a side note: During the writeup of the README and reviewing Prof. Hill's notes on super loops, it was realized that a lot of the functionality of zOS could likely be duplicated by creating a simple super loop structure...

 * I have coined zOS's method of executing tasks to be *periodic-sequential*. "Periodic" because of the cyclic RUN/SLACK state transitions that are bound to wall time, and "sequential" because of the static sequence (i.e., priority) of executing the tasks (e.g., Task 1, Task 2, ... , Task 7). Thanks to my fellow Computer Engineer David for pointing this out to me.

### Why the SX?
The SX microcontroller happened to be the most at-hand microprocessor that I had. Parallax in the past sold a development board called the SX Tech, which I happened to have, along with around eight SX28's I purchased when they announced the EOL. The development board provided all of the required components to develop the OS, and a breadboard to attach external peripherals.

The software (a minimal IDE with a debugger) provided by Parallax is still available online, along with all of the required documentation. I also happened to own a copy of "Programming the SX Microcontroller" by Günther Daubach, which I also purchased when the EOL was announced. Many of the examples and tables of processor instructions proved most useful during development of the OS.

# References

 * Daubach, Günther. "Programming the SX Microcontroller: A Complete Guide". Parallax, Inc. 2004.  
 * Cooling,J.E. "Software Engineering for Real-Time Systems". Pearson Education. 2003.
 * "SX-Key/Blitz Development System Manual 2.0". Parallax, Inc. 2013.  [https://www.parallax.com/sites/default/files/downloads/SX-Key-Blitz-Manual-V2.0.pdf](https://www.parallax.com/sites/default/files/downloads/SX-Key-Blitz-Manual-V2.0.pdf).
 * "SX20AC/SX28AC Datasheet". Parallax, Inc. 2008. [https://www.parallax.com/sites/default/files/downloads/SX20AC-SX28AC-Datasheet-v1.7.pdf](https://www.parallax.com/sites/default/files/downloads/SX20AC-SX28AC-Datasheet-v1.7.pdf)
 * Hill,K.M. "The cxlib Library Version 0.21". University of Hartford. 2004. [http://uhaweb.hartford.edu/kmhill/projects/cxlib/index.htm](http://uhaweb.hartford.edu/kmhill/projects/cxlib/index.htm)
 * Hill,K.M. "UHCOE ECE332: Super Loops in 9S12 Assembly Language". University of Hartford. 2009. [http://uhaweb.hartford.edu/kmhill/suppnotes/SuperLoop/SuperLoop9S12.htm](http://uhaweb.hartford.edu/kmhill/suppnotes/SuperLoop/SuperLoop9S12.htm)
