Task3_TCB_PC      EQU $70
Task3_TCB_STATUS  EQU $71
Task3_TCB_FSR     EQU $72
Task3_TCB_Counter EQU $73

Task3_Period      EQU 5

;;;;;;;;;;
; MACROS ;
;;;;;;;;;;
load_rc1 MACRO                       ; Loads button value into W
  mov  w,  rc                        ; Button is pulled high by an external pullup
  and  w,  #%00000010                ; If button is pressed, Z flag will be 1
ENDM

;;;;;;;;;;;;;;;;;;;
; TASK VARIABLES  ;
; MAX. = 11 bytes ;
;;;;;;;;;;;;;;;;;;;
ORG $75

;;;;;;;;;;;;;;;
;; TASK CODE ;;
;;;;;;;;;;;;;;;
ORG TASK2_END

; Description: Checks to see if the button is pressed. If so,
; it updates a global register so Task 1 can see it. We don't
; need to clear the register--Task 1 will do that for us.
TASK3
  setb DEBUG_T3_PIN
  load_rc1                           ; See if button is pressed
  jnz  :done
:debounce                            ; On first detection of press, wait one task period for debounce
  clrb DEBUG_T3_PIN
  TaskSleep Task3_Period             ; ~40 milliseconds
  setb DEBUG_T3_PIN
  load_rc1                           ; See if button is pressed
  jnz  :done
:press_detected                      ; This is a valid press
  mov  button_press,  #$01           ; Update the global button_press register (RTC Task 1)
:wait_for_depress                    ; Don't allow this task to repeat until the button is released
  load_rc1                           ; See if button is pressed
  jnz  :done
  clrb DEBUG_T3_PIN
  TaskSleep Task3_Period             ; ~50 milliseconds
  setb DEBUG_T3_PIN
  jmp  :wait_for_depress
:done
  clrb DEBUG_T3_PIN
  TaskSleep Task3_Period
  jmp  TASK3
TASK3_END

Task3_PC_Ptr      EQU TASK3&$0FF
